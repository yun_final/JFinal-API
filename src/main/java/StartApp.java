import com.jfinal.server.undertow.UndertowServer;
import com.yunfinal.api.tpl.ApiConfig;

/**
 * 启动项目，右键菜单》 Run
 */
public class StartApp {
    public static void main(String[] args) {
        UndertowServer.start(ApiConfig.class);
    }
}
