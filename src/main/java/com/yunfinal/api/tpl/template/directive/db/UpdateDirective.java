package com.yunfinal.api.tpl.template.directive.db;

import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * #sqlPara 指令方便定义SqlPara  https://www.jfinal.com/doc/5-13
 * <p>
 * 定义：
 * #update(受影响行数返回结果的变量名)
 * 在此是SQL模版语法
 * #end
 * <p>
 * @author  杜福忠
 */
public class UpdateDirective extends SqlParaDirective {

    @Override
    protected Object getData(DbPro db, SqlPara sqlPara) {
        setDefaultName("result");
        //可扩展，或者指定数据源
        return db.update(sqlPara);
    }
}
